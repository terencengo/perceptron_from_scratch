import numpy as np
#To create our deep learning model
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, Flatten, Dropout, Conv2D
#To read the file letters.csv
import pandas as pd
#To download the images
from tensorflow.python.keras.applications.resnet50 import preprocess_input
from tensorflow.python.keras.preprocessing.image import load_img, img_to_array
from os.path import join
#To show the images
from IPython.display import Image, display
#To show the evolution of the model
import matplotlib.pyplot as plt

## Preparing the data

# Opening and reading the data
data = pd.read_csv('C:/Users/teren/letters.csv')

#print(data.columns)

# Stocking the paths to get to the images in an array
image_dir = 'C:/Users/teren/letters'
img_filenames = data.file.values
img_paths = [join(image_dir, filename) for filename in img_filenames]

"""
# Visualizing the images
for img_path in img_paths[:3]:
    display(Image(img_path))
"""

#Number of distinct labels
num_classes = 33

# Size of the images used in the data
image_size = 32

# Function to transform img_paths so that it can be used as an input to our model
def read_and_prep(img_paths, img_height=image_size, img_width=image_size):
    imgs = [ load_img(img_path, target_size=(img_height, img_width)) for img_path in img_paths]
    img_array = np.array([img_to_array(img) for img in imgs])
    output = preprocess_input(img_array)
    return output

#Features
X = read_and_prep(img_paths)

#Targets
y = data.label
# Series --> np.array with values from 0 to 32
y= y.values-1

"""
#The targets array must have as many columns as labels
y= keras.utils.to_categorical(y)
# Prb: the first colmun of y is useless
y= np.array([y[row][1:] for row in range (1650)])
"""



## Comparing different models

# A list to stock the acc_curves
acc_list=[]

# A dict to stock the different optimizers to test
optimizer_dict={"SGD":keras.optimizers.SGD(),
                "Adam": keras.optimizers.Adam(),
                "RMSprop": keras.optimizers.RMSprop(),
                "Adamax": keras.optimizers.Adamax(),
                "Nadam":keras.optimizers.Nadam()}

optimizer_keys = list(optimizer_dict.keys())
optimizer_items = list(optimizer_dict.values())

# Feeding and training the model
# Analyzing the impact of different optimizers on the model's evolution
for optimizer in optimizer_items:
    # Introducing the model
    model = Sequential()

    # First convolutional layer
    model.add(Conv2D(filters=50, kernel_size=(3,3), strides=2, activation='relu', input_shape=(image_size, image_size, 3)))
    # Second convolutional layer
    model.add(Conv2D(filters=30, kernel_size=(3,3), strides=2, activation='relu'))

    # Flattering the ranges of images to get appropriate inputs for the following neurons
    model.add(Flatten())

    model.add(Dense(40, activation='relu'))
    #Prediction layers (one for each class)
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    history = model.fit(X, y, batch_size=132, epochs=10, validation_split=0.2, shuffle=True)

    acc_curve = history.history["acc"]
    acc_list.append(acc_curve)


for i, acc_curve in enumerate(acc_list):
    plt.plot(acc_curve, label="optimizer:{}".format(optimizer_keys[i]))
plt.xlabel("epoch")
plt.ylabel("accuracy")
plt.legend()
plt.savefig("optimizers_performances.png")
plt.show()