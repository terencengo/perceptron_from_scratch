import numpy as np
import matplotlib.pyplot as plt
#import tensorflow as tf

#Inputs
def get_dataset():
    rows_per_class = 25
    #Sick people have negative rates
    features_sick = np.random.randn(rows_per_class,2) + np.array([-2,-2])
    #Healthy ones have positive rates 
    features_healthy = np.random.randn(rows_per_class,2) + np.array([2,2])
    features = np.concatenate((features_sick, features_healthy), axis=0)
    #Sick people = Class 0
    targets_sick = np.zeros((rows_per_class,1))
    #Healthy people = Class 1
    targets_healthy = np.ones((rows_per_class,1))
    targets = np.concatenate((targets_sick, targets_healthy), axis=0)
    return rows_per_class, features, targets

#Random weights & bias
def variables_initialization():
    weights=np.random.randn(2,1)
    bias=0
    return weights,bias
    
def pre_activation(weights,features,bias):
    return np.dot(features,weights)+ bias 

def activation(z):
    y = 1/(1+np.exp(-z))
    #The individual i is sick if y[i]<0.5, healthy on the other hand
    """
    for i in range(len(y)):
        if y[i]<0.5:
            y[i] = 0
        else:
            y[i] = 1
    """
    return y

# Defining random m0 and v0 to initialize the Adam relations
m=np.array([1,1,1])
v=1

def squarred_norm(g):
    result = 0
    for value in g:
        result = result + value**2
    return result

def define_m(m, g1, beta1=0.8):
    return [beta1*value for value in m] + [(1-beta1)*elem for elem in g1]
    

def define_v(v, g2, beta2=0.5):
    return beta2*v + (1-beta2)*g2
    
# From m to m_chapeau
def transform_m(m, t, beta1=0.8):
    return [value/(1-beta1**t) for value in m]

# From v to v_chapeau
def transform_v(v, t, beta2=0.5): 
    return v/(1-beta2**t) 

# m_chapeau and v_chapeau are called m_ and v_
def update_theta(theta, m_, v_, learning_rate=0.1, epsilon=10**(-8)):
    l = []
    for i in range(len(theta)):
        l.append(theta[i] - learning_rate/(np.sqrt(v_) + epsilon)*m_[i])
    return l


#Error of our predictions
def e(y,targets):
    return np.mean((y-targets)**2)

#Partial derivates of error function (features & targets are constants)    
def partial_derivate_w1(weights, bias, features, targets):
    gradient_w1 =0
    z = pre_activation(weights,features, bias)
    for i in range(len(features)):
        gradient_w1+=(activation(z[i])-targets[i])*activation(z[i])*activation(-z[i])*features[i][0]
    return gradient_w1*(2/len(features))
    
    
def partial_derivate_w2(weights, bias, features, targets):
    gradient_w2 =0
    z = pre_activation(weights,features, bias)
    for i in range(len(features)):
        gradient_w2+=(activation(z[i])-targets[i])*activation(z[i])*activation(-z[i])*features[i][1]
    return gradient_w2*(2/len(features))
    
    
def partial_derivate_b(weights, bias, features, targets):
    gradient_b =0
    z = pre_activation(weights,features, bias)
    for i in range(len(features)):
        gradient_b+=(activation(z[i])-targets[i])*activation(z[i])*activation(-z[i])
    return gradient_b*(2/len(features))
    

if __name__ == '__main__':
    rows_per_class, features, targets = get_dataset()
    weights, bias = variables_initialization()
    z = pre_activation(weights, features, bias)
    y = activation(z)

    #Training the model with the Adam optimizer model
    
    for epoch in range(100):
        # The vector containing the values of the model's parameters
        theta = [weights[0], weights[1], bias]
        
        #Gradient values
        gradient_w1 = partial_derivate_w1(weights, bias, features, targets)
        gradient_w2= partial_derivate_w2(weights, bias, features, targets)
        gradient_b= partial_derivate_b(weights, bias, features, targets)
        
        # The gradient
        gradient1=[gradient_w1, gradient_w2, gradient_b]
        # The squarred norm of the gradient
        gradient2 = squarred_norm(gradient1)
        
        # Defining m and v
        m = define_m(m, g1=gradient1, beta1=0.8)
        v = define_v(v, g2=gradient2, beta2=0.5)
        
        # Turning them into m_chapeau and v_chapeau
        m_ = transform_m(m,  t=epoch, beta1=0.8)
        v_ = transform_v(v,  t=epoch, beta2=0.5)
        
        #Adam optimizer method
        theta = update_theta(theta, m_, v_, learning_rate=0.1, epsilon=10**(-8))

        #New computation of the predictions
        predictions =activation(pre_activation(theta[:2], features, theta[2]))
        #Check the cost for each epoch
        print("Error:{}".format(e(predictions, targets)))
        
        
        """Plot the evolution of the decision boundary (only works well under Spyder)
        if y>0.5 healthy
        else sick
        ie the limit condition is: A(z=w1*x1+w2*x2)=0.5 => w1*x1+w2*x2=0
        """
        boundary_points = np.zeros([2*rows_per_class,])
        for i in range(2*rows_per_class):
            boundary_points[i] = (-theta[0]*features[i][0])/theta[1]
        
        plt.figure(figsize=(7,7))
        plt.scatter(features[:rows_per_class,0], features[:rows_per_class,1], label='Sick', c='red')
        plt.scatter(features[rows_per_class:,0], features[rows_per_class:,1], label='Healthy')
        plt.plot(features[:,0], boundary_points, label='Decision boundary')
        plt.xlabel('Taux de globules blancs')
        plt.ylabel('Taux de globules rouges')
        plt.legend()
        plt.show()
        